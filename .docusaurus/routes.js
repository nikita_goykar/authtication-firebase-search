import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/authtication-firebase-search/__docusaurus/debug',
    component: ComponentCreator('/authtication-firebase-search/__docusaurus/debug', 'aae'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/__docusaurus/debug/config',
    component: ComponentCreator('/authtication-firebase-search/__docusaurus/debug/config', '272'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/__docusaurus/debug/content',
    component: ComponentCreator('/authtication-firebase-search/__docusaurus/debug/content', '85e'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/__docusaurus/debug/globalData',
    component: ComponentCreator('/authtication-firebase-search/__docusaurus/debug/globalData', '762'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/__docusaurus/debug/metadata',
    component: ComponentCreator('/authtication-firebase-search/__docusaurus/debug/metadata', '2cb'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/__docusaurus/debug/registry',
    component: ComponentCreator('/authtication-firebase-search/__docusaurus/debug/registry', 'a7d'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/__docusaurus/debug/routes',
    component: ComponentCreator('/authtication-firebase-search/__docusaurus/debug/routes', '471'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog',
    component: ComponentCreator('/authtication-firebase-search/blog', '4c5'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/archive',
    component: ComponentCreator('/authtication-firebase-search/blog/archive', '3c6'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/first-blog-post',
    component: ComponentCreator('/authtication-firebase-search/blog/first-blog-post', '3e9'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/long-blog-post',
    component: ComponentCreator('/authtication-firebase-search/blog/long-blog-post', '099'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/mdx-blog-post',
    component: ComponentCreator('/authtication-firebase-search/blog/mdx-blog-post', '70b'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/tags',
    component: ComponentCreator('/authtication-firebase-search/blog/tags', 'f0c'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/tags/docusaurus',
    component: ComponentCreator('/authtication-firebase-search/blog/tags/docusaurus', 'f37'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/tags/facebook',
    component: ComponentCreator('/authtication-firebase-search/blog/tags/facebook', '569'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/tags/hello',
    component: ComponentCreator('/authtication-firebase-search/blog/tags/hello', '93d'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/tags/hola',
    component: ComponentCreator('/authtication-firebase-search/blog/tags/hola', 'fcc'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/blog/welcome',
    component: ComponentCreator('/authtication-firebase-search/blog/welcome', '9ce'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/markdown-page',
    component: ComponentCreator('/authtication-firebase-search/markdown-page', '238'),
    exact: true
  },
  {
    path: '/authtication-firebase-search/docs',
    component: ComponentCreator('/authtication-firebase-search/docs', '44d'),
    routes: [
      {
        path: '/authtication-firebase-search/docs/category/tutorial---basics',
        component: ComponentCreator('/authtication-firebase-search/docs/category/tutorial---basics', 'e3a'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/category/tutorial---extras',
        component: ComponentCreator('/authtication-firebase-search/docs/category/tutorial---extras', '5e0'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/intro',
        component: ComponentCreator('/authtication-firebase-search/docs/intro', '868'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/tutorial-basics/congratulations',
        component: ComponentCreator('/authtication-firebase-search/docs/tutorial-basics/congratulations', '80f'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/tutorial-basics/create-a-blog-post',
        component: ComponentCreator('/authtication-firebase-search/docs/tutorial-basics/create-a-blog-post', 'f03'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/tutorial-basics/create-a-document',
        component: ComponentCreator('/authtication-firebase-search/docs/tutorial-basics/create-a-document', '5c7'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/tutorial-basics/create-a-page',
        component: ComponentCreator('/authtication-firebase-search/docs/tutorial-basics/create-a-page', 'f63'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/tutorial-basics/deploy-your-site',
        component: ComponentCreator('/authtication-firebase-search/docs/tutorial-basics/deploy-your-site', '18e'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/tutorial-basics/markdown-features',
        component: ComponentCreator('/authtication-firebase-search/docs/tutorial-basics/markdown-features', '0b9'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/tutorial-extras/manage-docs-versions',
        component: ComponentCreator('/authtication-firebase-search/docs/tutorial-extras/manage-docs-versions', 'e1e'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/authtication-firebase-search/docs/tutorial-extras/translate-your-site',
        component: ComponentCreator('/authtication-firebase-search/docs/tutorial-extras/translate-your-site', 'db8'),
        exact: true,
        sidebar: "tutorialSidebar"
      }
    ]
  },
  {
    path: '/authtication-firebase-search/',
    component: ComponentCreator('/authtication-firebase-search/', 'd5f'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
